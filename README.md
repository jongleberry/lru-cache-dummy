# LRU Cache Dummy

A cache that implements the most basic of [lru-cache](https://github.com/isaacs/node-lru-cache)'s interface.
However, it doesn't actually evict anything.
Use this for development or when you don't actually care about memory usage,
but want to be able to plug in `lru-cache` very easily.
